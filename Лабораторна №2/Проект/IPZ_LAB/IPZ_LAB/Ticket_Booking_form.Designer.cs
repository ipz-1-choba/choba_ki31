﻿namespace IPZ_LAB
{
    partial class Ticket_Booking_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            this.Cancel_button = new System.Windows.Forms.Button();
            this.Back_button = new System.Windows.Forms.Button();
            this.Exit_button = new System.Windows.Forms.Button();
            this.Reserve_button = new System.Windows.Forms.Button();
            this.Buy_button = new System.Windows.Forms.Button();
            this.Hide_button = new System.Windows.Forms.Button();
            this.s1_1 = new System.Windows.Forms.Button();
            this.s1_2 = new System.Windows.Forms.Button();
            this.s1_3 = new System.Windows.Forms.Button();
            this.s1_4 = new System.Windows.Forms.Button();
            this.s2_1 = new System.Windows.Forms.Button();
            this.s2_2 = new System.Windows.Forms.Button();
            this.s2_3 = new System.Windows.Forms.Button();
            this.s2_4 = new System.Windows.Forms.Button();
            this.s3_1 = new System.Windows.Forms.Button();
            this.s3_2 = new System.Windows.Forms.Button();
            this.s3_3 = new System.Windows.Forms.Button();
            this.s3_4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label1.ForeColor = System.Drawing.Color.Snow;
            label1.Location = new System.Drawing.Point(29, 154);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(68, 33);
            label1.TabIndex = 29;
            label1.Text = "Ряд 1";
            // 
            // label2
            // 
            label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label2.ForeColor = System.Drawing.Color.Snow;
            label2.Location = new System.Drawing.Point(29, 218);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 33);
            label2.TabIndex = 30;
            label2.Text = "Ряд 2";
            // 
            // label3
            // 
            label3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label3.ForeColor = System.Drawing.Color.Snow;
            label3.Location = new System.Drawing.Point(29, 281);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(68, 33);
            label3.TabIndex = 31;
            label3.Text = "Ряд 3";
            // 
            // Cancel_button
            // 
            this.Cancel_button.BackColor = System.Drawing.Color.White;
            this.Cancel_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cancel_button.FlatAppearance.BorderSize = 0;
            this.Cancel_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancel_button.Location = new System.Drawing.Point(701, 0);
            this.Cancel_button.Name = "Cancel_button";
            this.Cancel_button.Size = new System.Drawing.Size(99, 30);
            this.Cancel_button.TabIndex = 4;
            this.Cancel_button.Text = "Закрити\r\n";
            this.Cancel_button.UseVisualStyleBackColor = false;
            this.Cancel_button.Click += new System.EventHandler(this.Cancel_button_Click);
            // 
            // Back_button
            // 
            this.Back_button.BackColor = System.Drawing.Color.SteelBlue;
            this.Back_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Back_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Back_button.ForeColor = System.Drawing.Color.White;
            this.Back_button.Location = new System.Drawing.Point(0, 407);
            this.Back_button.Name = "Back_button";
            this.Back_button.Size = new System.Drawing.Size(146, 43);
            this.Back_button.TabIndex = 5;
            this.Back_button.Text = "НАЗАД";
            this.Back_button.UseVisualStyleBackColor = false;
            this.Back_button.Click += new System.EventHandler(this.Back_button_Click);
            // 
            // Exit_button
            // 
            this.Exit_button.BackColor = System.Drawing.Color.SteelBlue;
            this.Exit_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit_button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Exit_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Exit_button.ForeColor = System.Drawing.Color.White;
            this.Exit_button.Location = new System.Drawing.Point(139, 407);
            this.Exit_button.Name = "Exit_button";
            this.Exit_button.Size = new System.Drawing.Size(142, 43);
            this.Exit_button.TabIndex = 6;
            this.Exit_button.Text = "ВИХІД";
            this.Exit_button.UseVisualStyleBackColor = false;
            this.Exit_button.Click += new System.EventHandler(this.Exit_button_Click);
            // 
            // Reserve_button
            // 
            this.Reserve_button.BackColor = System.Drawing.Color.SteelBlue;
            this.Reserve_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Reserve_button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Reserve_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Reserve_button.ForeColor = System.Drawing.Color.White;
            this.Reserve_button.Location = new System.Drawing.Point(529, 347);
            this.Reserve_button.Name = "Reserve_button";
            this.Reserve_button.Size = new System.Drawing.Size(271, 43);
            this.Reserve_button.TabIndex = 7;
            this.Reserve_button.Text = "ЗАБРОНЮВАТИ";
            this.Reserve_button.UseVisualStyleBackColor = false;
            this.Reserve_button.Click += new System.EventHandler(this.Reserve_button_Click);
            // 
            // Buy_button
            // 
            this.Buy_button.BackColor = System.Drawing.Color.SteelBlue;
            this.Buy_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Buy_button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Buy_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Buy_button.ForeColor = System.Drawing.Color.White;
            this.Buy_button.Location = new System.Drawing.Point(529, 407);
            this.Buy_button.Name = "Buy_button";
            this.Buy_button.Size = new System.Drawing.Size(271, 43);
            this.Buy_button.TabIndex = 8;
            this.Buy_button.Text = "СПЛАТИТИ";
            this.Buy_button.UseVisualStyleBackColor = false;
            this.Buy_button.Click += new System.EventHandler(this.Buy_button_Click);
            // 
            // Hide_button
            // 
            this.Hide_button.BackColor = System.Drawing.Color.White;
            this.Hide_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Hide_button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Hide_button.Location = new System.Drawing.Point(665, 0);
            this.Hide_button.Name = "Hide_button";
            this.Hide_button.Size = new System.Drawing.Size(30, 30);
            this.Hide_button.TabIndex = 11;
            this.Hide_button.Text = "_";
            this.Hide_button.UseVisualStyleBackColor = false;
            this.Hide_button.Click += new System.EventHandler(this.Hide_button_Click);
            // 
            // s1_1
            // 
            this.s1_1.Location = new System.Drawing.Point(115, 138);
            this.s1_1.Name = "s1_1";
            this.s1_1.Size = new System.Drawing.Size(75, 49);
            this.s1_1.TabIndex = 17;
            this.s1_1.Text = "1";
            this.s1_1.UseVisualStyleBackColor = true;
            this.s1_1.Click += new System.EventHandler(this.s1_1_Click);
            // 
            // s1_2
            // 
            this.s1_2.Location = new System.Drawing.Point(210, 138);
            this.s1_2.Name = "s1_2";
            this.s1_2.Size = new System.Drawing.Size(75, 49);
            this.s1_2.TabIndex = 18;
            this.s1_2.Text = "2";
            this.s1_2.UseVisualStyleBackColor = true;
            this.s1_2.Click += new System.EventHandler(this.s1_2_Click);
            // 
            // s1_3
            // 
            this.s1_3.Location = new System.Drawing.Point(307, 138);
            this.s1_3.Name = "s1_3";
            this.s1_3.Size = new System.Drawing.Size(75, 49);
            this.s1_3.TabIndex = 19;
            this.s1_3.Text = "3";
            this.s1_3.UseVisualStyleBackColor = true;
            this.s1_3.Click += new System.EventHandler(this.s1_3_Click);
            // 
            // s1_4
            // 
            this.s1_4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.s1_4.Location = new System.Drawing.Point(397, 138);
            this.s1_4.Name = "s1_4";
            this.s1_4.Size = new System.Drawing.Size(75, 49);
            this.s1_4.TabIndex = 20;
            this.s1_4.Text = "button4";
            this.s1_4.UseVisualStyleBackColor = false;
            this.s1_4.Click += new System.EventHandler(this.s1_4_Click);
            // 
            // s2_1
            // 
            this.s2_1.Location = new System.Drawing.Point(115, 202);
            this.s2_1.Name = "s2_1";
            this.s2_1.Size = new System.Drawing.Size(75, 49);
            this.s2_1.TabIndex = 21;
            this.s2_1.Text = "button5";
            this.s2_1.UseVisualStyleBackColor = true;
            this.s2_1.Click += new System.EventHandler(this.s2_1_Click);
            // 
            // s2_2
            // 
            this.s2_2.Location = new System.Drawing.Point(210, 202);
            this.s2_2.Name = "s2_2";
            this.s2_2.Size = new System.Drawing.Size(75, 49);
            this.s2_2.TabIndex = 22;
            this.s2_2.Text = "button6";
            this.s2_2.UseVisualStyleBackColor = true;
            this.s2_2.Click += new System.EventHandler(this.s2_2_Click);
            // 
            // s2_3
            // 
            this.s2_3.Location = new System.Drawing.Point(307, 202);
            this.s2_3.Name = "s2_3";
            this.s2_3.Size = new System.Drawing.Size(75, 49);
            this.s2_3.TabIndex = 23;
            this.s2_3.Text = "button7";
            this.s2_3.UseVisualStyleBackColor = true;
            this.s2_3.Click += new System.EventHandler(this.s2_3_Click);
            // 
            // s2_4
            // 
            this.s2_4.Location = new System.Drawing.Point(397, 202);
            this.s2_4.Name = "s2_4";
            this.s2_4.Size = new System.Drawing.Size(75, 49);
            this.s2_4.TabIndex = 24;
            this.s2_4.Text = "button8";
            this.s2_4.UseVisualStyleBackColor = true;
            this.s2_4.Click += new System.EventHandler(this.s2_4_Click);
            // 
            // s3_1
            // 
            this.s3_1.Location = new System.Drawing.Point(115, 265);
            this.s3_1.Name = "s3_1";
            this.s3_1.Size = new System.Drawing.Size(75, 49);
            this.s3_1.TabIndex = 25;
            this.s3_1.Text = "button9";
            this.s3_1.UseVisualStyleBackColor = true;
            this.s3_1.Click += new System.EventHandler(this.s3_1_Click);
            // 
            // s3_2
            // 
            this.s3_2.Location = new System.Drawing.Point(210, 265);
            this.s3_2.Name = "s3_2";
            this.s3_2.Size = new System.Drawing.Size(75, 49);
            this.s3_2.TabIndex = 26;
            this.s3_2.Text = "button10";
            this.s3_2.UseVisualStyleBackColor = true;
            this.s3_2.Click += new System.EventHandler(this.s3_2_Click);
            // 
            // s3_3
            // 
            this.s3_3.Location = new System.Drawing.Point(307, 265);
            this.s3_3.Name = "s3_3";
            this.s3_3.Size = new System.Drawing.Size(75, 49);
            this.s3_3.TabIndex = 27;
            this.s3_3.Text = "1";
            this.s3_3.UseVisualStyleBackColor = true;
            this.s3_3.Click += new System.EventHandler(this.s3_3_Click);
            // 
            // s3_4
            // 
            this.s3_4.Location = new System.Drawing.Point(397, 265);
            this.s3_4.Name = "s3_4";
            this.s3_4.Size = new System.Drawing.Size(75, 49);
            this.s3_4.TabIndex = 28;
            this.s3_4.Text = "button12";
            this.s3_4.UseVisualStyleBackColor = true;
            this.s3_4.Click += new System.EventHandler(this.s3_4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::IPZ_LAB.Properties.Resources.ticket_booking_background1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 450);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // Ticket_Booking_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.s3_4);
            this.Controls.Add(this.s3_3);
            this.Controls.Add(this.s3_2);
            this.Controls.Add(this.s3_1);
            this.Controls.Add(this.s2_4);
            this.Controls.Add(this.s2_3);
            this.Controls.Add(this.s2_2);
            this.Controls.Add(this.s2_1);
            this.Controls.Add(this.s1_4);
            this.Controls.Add(this.s1_3);
            this.Controls.Add(this.s1_2);
            this.Controls.Add(this.s1_1);
            this.Controls.Add(this.Hide_button);
            this.Controls.Add(this.Buy_button);
            this.Controls.Add(this.Reserve_button);
            this.Controls.Add(this.Exit_button);
            this.Controls.Add(this.Back_button);
            this.Controls.Add(this.Cancel_button);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Ticket_Booking_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ticket_Booking_form";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Cancel_button;
        private System.Windows.Forms.Button Back_button;
        private System.Windows.Forms.Button Exit_button;
        private System.Windows.Forms.Button Reserve_button;
        private System.Windows.Forms.Button Buy_button;
        private System.Windows.Forms.Button Hide_button;
        private System.Windows.Forms.Button s1_1;
        private System.Windows.Forms.Button s1_2;
        private System.Windows.Forms.Button s1_3;
        private System.Windows.Forms.Button s1_4;
        private System.Windows.Forms.Button s2_1;
        private System.Windows.Forms.Button s2_2;
        private System.Windows.Forms.Button s2_3;
        private System.Windows.Forms.Button s2_4;
        private System.Windows.Forms.Button s3_1;
        private System.Windows.Forms.Button s3_2;
        private System.Windows.Forms.Button s3_3;
        private System.Windows.Forms.Button s3_4;
    }
}