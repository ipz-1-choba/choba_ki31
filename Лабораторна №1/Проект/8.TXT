@startuml
start
:Надіслати запит для купівлі квитка>
:Вибрати дату,годину та фільм;
:Отримати повідомлення про оплату<
repeat:Надіслати запит з оплатою квитка>
:Чекаємо обробки запиту 3с;
backward: Повідомлення помилки;
repeat while (Квиток оплачено) is (ні) not (так)
:Отримати повідомлення про успішно куплений квиток<
stop
@enduml